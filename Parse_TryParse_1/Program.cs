﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parse_TryParse_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //parsing is useful when you want to convert from one type to another
            //lets take this string variable
            string hello_there = "5";  //this one is obviously a number
            string hello_there_2 = "what"; //this one is not

            int temp_1 = int.Parse(hello_there);  //this will succeed without problems. because we have a int in the string

            try
            {
                int temp_2 = int.Parse(hello_there_2); //this will raise an exception...which is why I have put it in a try block
            }
            catch
            {
                Console.WriteLine("exception thrown");
            }

            //same as above but instead of Parse, I will use tryparse
            //this will check on its own if conversion is possible. if not, it will just leave it there instead of throwing an exception
            //the output will be stored in the result_of_parse variable
            int result_of_parse = 0;
            int.TryParse(hello_there,out result_of_parse);
            Console.WriteLine("Converted number with try parse is {0} string is {1}", result_of_parse, hello_there);
            result_of_parse = 0;
            int.TryParse(hello_there_2, out result_of_parse); //no exception thrown unlike the parse method above.
            Console.WriteLine("Converted number with try parse is {0} string is {1}", result_of_parse,hello_there_2);


            //now, if I want, I can find out if the conversion was successfull or not
            bool conversion_success = int.TryParse(hello_there_2, out result_of_parse);
            if(conversion_success == true)
            {
                Console.WriteLine("conversion was successfull for string {0}",hello_there_2);
            }
            else
            {
                Console.WriteLine("conversion was NOT successfull for string {0}", hello_there_2);
            }


            // now lets look at Convert method
            //remember that parse only works on string types
            //convert works between all base types, and yeah, you will have to check on line or base types 
            double temp_5 = 50.8765;
            int temp_6 = Convert.ToInt32(temp_5);  //when you do this, convert lops off everything to the right of the decimal point

            Console.WriteLine(" double is {0} and converted int is {1}", temp_5, temp_6);

            //lets not allow the console window to dissapear
            Console.ReadLine();
        }
    }
}
